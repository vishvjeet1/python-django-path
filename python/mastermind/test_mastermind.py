from mastermind import MasterMind


def test_mastermind():
    """
    1. Try different answers such as 99999, 0, 53030
    2. What if the input has 10 digits?
    3. What if the input is not a number?

    """
    m = MasterMind()
    m.start_game()
    m._answer = 10000
    guess_output = m.guess(55500)
    expected_output = {"is_correct": False, "cows": 0, "bulls": 2}

    assert guess_output == expected_output

    guess_output = m.guess(10000)
    expected_output = {"is_correct": True, "cows": 0, "bulls": 5}

    assert guess_output == expected_output

    m.reset()

    m._answer = 40440

    # etc...

test_mastermind()
