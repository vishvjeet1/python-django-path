## Resources

* https://realpython.com/introduction-to-python-generators/
* https://medium.freecodecamp.org/how-and-why-you-should-use-python-generators-f6fb56650888

Refer other resources if necessary

## Drills

* Define a `gen_range` function that works like the builtin `range` function. It should yield a number in each iteration

```
def gen_range(start, stop, step):
    pass
```

* Define a `zip` function that works similar to the builtin `zip` function. (Unlike the builtin `zip`, assume that all iterables are of the same size)

```
def gen_zip(*iterables):
    pass
```
