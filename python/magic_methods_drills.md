## Magic methods


### Resources

* https://dbader.org/blog/python-dunder-methods
* https://stackoverflow.com/questions/2657627/why-does-python-use-magic-methods
* https://www.tutorialsteacher.com/python/magic-methods-in-python

__________

### Drills

**Warmup**

* Add two integers using a suitable magic method
* Find the length of a list using a suitable magic method
* Find the length of a string using a suitable magic method
* Add two lists by calling a magic method on the first list

__________


**Sortable and comparable classes:**

i) Create a class, `BankAccount` as below

```
class BankAccount:
    def __init__(self, id, balance):
        self.id = id
        self.balance = balance

    def __repr__(self):
        return "BankAccount(id={}, balance={})".format(self.id, self.balance)
```

ii) Create a list of BankAccounts

```
import random
accounts = [BankAccount(id=i, balance=random.randint(1, 10000)) for i in range(100)]
```

iii) Sort `accounts` by balance, using suitable magic methods

iv) Compare whether two bank accounts are equal. Two bank accounts are equal if they have the same `id` and `balance`

```
a1 = BankAccount(id=1, balance=100)
a2 = BankAccount(id=1, balance=100)
a3 = BankAccount(id=2, balance=100)

a1 == a2 # True
a1 == a3 # False
```

___________

**Custom Iterator**

Define a class `Bank`, which contains `BankAccount`s

```
class Bank:
    def __init__(self):
        self.accounts = []

    def add_account(self, account):
        self.accounts.append(account)
```

Use a magic method so that you can iterate through the accounts in the bank using the below syntax

```
bank = Bank()
for i in range(100):
    bank.add_account(BankAccount(id=i, balance=i * 100))

for account in bank:
    print(account)
```

