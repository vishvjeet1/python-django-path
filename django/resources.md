# Django

### Tutorials

**Official Tutorial**

https://docs.djangoproject.com/en/2.2/intro/tutorial01/

**Django girls**

https://tutorial.djangogirls.org/en/



__________

### HTTP request-response lifecycle

* Video - https://www.youtube.com/watch?v=q0YqAbI7rw4
* High-level visualization - (Does not include middlewares) - https://www.youtube.com/watch?v=RLo9RJhbOrQ
* More details - http://django-easy-tutorial.blogspot.com/2017/03/django-request-lifecycle.html
* Detailed visualization - https://learnbatta.com/course/django/understanding-request-response-lifecycle-in-django/


__________________

### Database and Model Design

* Normalization: http://en.wikipedia.org/wiki/Database_normalization

* What not to store in a database: https://www.revsys.com/tidbits/three-things-you-should-never-put-your-database/

* Designing better models: https://simpleisbetterthancomplex.com/tips/2018/02/10/django-tip-22-designing-better-models.html

* Transactions
    - https://en.wikipedia.org/wiki/Database_transaction
    - https://vladmihalcea.com/a-beginners-guide-to-acid-and-database-transactions/
    - Django transaction management - https://realpython.com/transaction-management-with-django-1-6/ (Check if the points are valid for Django 2.x)

_______________________

### ORM - Basics, QuerySets, Model Managers

**Viewing SQL queries executed by the ORM**

```
import logging
l = logging.getLogger('django.db.backends')
l.setLevel(logging.DEBUG)
l.addHandler(logging.StreamHandler())
```


**Resources**

**Basics**: https://www.codementor.io/overiq/basics-of-django-orm-cwamhcerp

#### Querysets

- https://docs.djangoproject.com/en/2.1/ref/models/querysets/ (Understand that querysets are lazily evaluated. No need to learn the entire API. Just skim through all the features and refer this page when required)

#### `select_related` and `prefetch_related`

- `n+1` query problem - https://stackoverflow.com/questions/97197/what-is-the-n1-selects-problem-in-orm-object-relational-mapping
- https://medium.com/@lucasmagnum/djangotip-select-prefetch-related-e76b683aa457
- https://stackoverflow.com/questions/31237042/whats-the-difference-between-select-related-and-prefetch-related-in-django-orm

Query Expressions - https://docs.djangoproject.com/en/1.11/ref/models/expressions/

#### Model managers

* https://simpleisbetterthancomplex.com/tips/2016/08/16/django-tip-11-custom-manager-with-chainable-querysets.html

* More details - https://docs.djangoproject.com/en/2.1/topics/db/managers/

__________________


