* Dynamically typed
* Mutable and immutable types
* References, variables, object id
* Dictionary - O(1) operations
* Lists - Dynamic array
* Strings - immutable
* Classes - Session
* modules
* What happens when you import a module or a function in a module
* What's a package - Show matplotlib example


