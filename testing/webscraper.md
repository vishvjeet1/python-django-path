### Build a test suite for the webscraper project

Checklist:
* Run your test on a local server which has a bunch of HTML files that link to each other
* Reset your environment after running each test, i.e. delete all the contents in the database
* Work on your **existing** webscraper repo.
* Include instructions on how to run the tests
* Reach 95% test coverage using `coverage.py` - https://coverage.readthedocs.io/en/v4.5.x/
* Ensure there are no `flake8` errors.


