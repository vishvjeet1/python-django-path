# 💬 Project - Chat App


## Build a Chat app using Pusher, Sandman2

Pusher - https://pusher.com/
Sandman2 - https://github.com/jeffknupp/sandman2

## Prereqs

* Callbacks
* Closures
* DOM manipulations using JavaScript
* HTTP - Methods, Status Codes
* Postman - Fetch and create resources (channels, messages)
* Making Ajax requests using either jQuery or `fetch`

## Features:

### Home Page
1. Lists all channels
2. User can also create a channel

### Channel Page
1. Lists all messages belonging to channel.
2. User can also add messages to channel.
3. When a message is added, it will be broadcasted to everyone on the channel. 


### Detailed requirements:

* On the home page, fetch all channels by making a `GET` request to Sandman. Each channel should have the url `/channels/<channel_name>`
* Ask user to enter a `username` as soon as they visit the site.
* Have a form to add a channel. The channel should be added by making an ajax `POST` request to Sandman. After the channel is added, it should be added to the list of channels
* In the channels page, fetch all messages belonging to channel by making a `GET` request to Sandman2.
* When user adds a message, first store it in the database by making a `POST` request to Sandman, and then broadcast the message using Pusher
* Listen to messages from other users using Pusher, and display them as and when you receive messages. 

### Sandman2:

* Use `sqlite3` (Don't waste time setting up your database)

* Create the following tables

```sql

CREATE TABLE channels 
  ( 
     id   INTEGER PRIMARY KEY AUTOINCREMENT, 
     name VARCHAR(255) 
  ); 

CREATE TABLE messages 
  ( 
     id          INTEGER PRIMARY KEY AUTOINCREMENT, 
     username    VARCHAR(255), 
     text        VARCHAR(255), 
     channel_id INTEGER, 
     FOREIGN KEY(channel_id) REFERENCES channels(id) 
  );
```


#### Setting up CORS

* You need to modify Sandman2's source. 

1. Install [`flask-cors`](https://flask-cors.readthedocs.io/en/latest/)

2. In `sandman2`'s source directory, open `app.py`

3. In `app.py`, import `CORS`

```
from flask_cors import CORS
```

4. In `app.py`, you'll find the below line:

```
app = Flask('sandman2')
```

Add the following line after the above line:

```
CORS(app)
```

Restart Sandman
__________

### Pusher and real-time-communication

#### Workflow

1. On the client, subscribe to messages on a pusher channel.
2. On the client, when a message is added to a channel
    1. Make an AJAX request to store the message in the database
    2. Once you receive a response, make an AJAX request to `/broadcast`
3. When the Sandman server receives a request to `/broadcast`, it will trigger a pusher event. A pusher server (NOT managed by us) will receive the event, and broadcast to all the clients.
4. Each client receive the event and then display the message IF and ONLY IF the message was added to the current channel.

#### Project Setup

1. Create an account on Pusher. 
2. Create a "channels app" on Pusher. 
3. Choose "Vanilla JavaScript" as your client framework, and "Python" as your backend language.
4. Replace the contents of Sandman2's `app.py` with `chat_app.py` (https://gitlab.com/pramod8/python-django-path/blob/master/chat_app.py)
5. In `chat_app.py`, you'll find a variable, `pusher_client`. Add your API keys here.

__________________

### Coding Standards:

#### Create modules for

* Ajax Stuff - Interacting with the server. Do you remember how you had a `database` module in the `webscraper` project? Have a similar module for Ajax. 
* UI - Listening to user input, adding elements to the DOM etc.

**Write small functions**

### Functions - Follow the Unix philosophy 

* Write functions that do one thing and do it well.
* Write functions that work well together

### Linting / Formatting

* Use prettier - https://prettier.io/
* Use `ESLint`

Your code shouldn't have any warnings


### Common Checklist


0. Write modular code. Classify the functions into categories like

    1. UI related
    2. Server interaction
    3. Event handlers

```
function saveMessage(username, text) {
    // make ajax request
}

function broadcastMessage(username, text) {
    // another ajax request
}

function saveAndBroadcastMessage(username, text) {
    saveMessage(username, text)
    broadcastMessage(username, text)
}

function appendMessageToChannel(username, text) {
    $("<li>") // etc... to create message html element
}


$("#send-btn").click(function() {
    appendMessageToChannel(username, text)
    saveAndBroadcastMessage(username, text);
})
```

1. Follow the DRY principle - https://en.wikipedia.org/wiki/Don%27t_repeat_yourself. i.e don't write duplicate logic to perform common tasks such as creating message elements etc.

2. Show the user who typed the message. Ask for a username in the home page. And use that variable when storing and broadcasting the message. Store the username in the messages table.

3. Highlight the current channel.

4. Make UI responsive

5. Add a cursor to clickable items. Add `cursor: pointer`. 

6. Use method chaining when creating jQuery elements

Example:

```
$("<p>")
    .attr("channel_id", 32323)
    .text("Hello world")
    .addClass("hello world")
```


7. Think about your workflow, and constantly see how you can speed up



