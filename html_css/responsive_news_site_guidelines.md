## Guidelines:

Read following sections from https://learn.shayhowe.com

* Writing Your Best Code
* Performance & Organization

Create a directory called `responsive_news_site`.

Your directory-structure should have the following files:

```
responsive_news_site
├── app.css
├── index.html
```

* `index.html` - Contains all the HTML code
* `app.css` - Contains all the the CSS rules

_________________


### Pay close attention to detail.

Mockups:

`sm`: Small screens; `md`: Medium sized screens; `lg`: Large screens


* Align all the components properly, according to the mockups.
* Add appropriate whitespace
* Use a `sans-serif` font - Choose one from [Google Fonts](https://fonts.google.com/)

### Responsiveness

* Test your site on various screen sizes.

Add the following tag in the `head`

```html
<meta name="viewport" content="width=device-width, initial-scale=1">
```
Read 
* https://css-tricks.com/snippets/html/responsive-meta-tag/
* https://developer.mozilla.org/en-US/docs/Mozilla/Mobile/Viewport_meta_tag

Use the following breakpoints:

```css
/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 480px) {...}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {...}

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {...} 
}
```


### Give descriptive names:

**Good**

```html
<ul className='headlines'>
</ul>
```

**Bad**

```html
<ul className='a'>
</ul>

<div className='b'>
</div>
```

### Follow a consistent naming convention

* CSS names must be in lowercase and words must be `hyphen-separated`. 
* Don't write inline CSS


**Good**

```html
<li className='headline-item'>
</li>

<div className='weather-widget'>
</div>
```

**Bad**

```html
<ul className='headlineItem'>
</ul>

<div className='Weather-Widget'>
</div>
```

### Don't repeat code for each layout

* Use appropriate responsive queries instead.

### Use a code formatter

* Use a HTML/CSS prettifier extension and make your code presentable.


__________________

## Common Checklist

* Don't hardcode pixel values. Give logical rules that work on all resolutions instead - (Give hero image caption example).
* Don't use selectors such as `li`, `ul`, `section` to style individual components. Use specific selectors instead.
* Use a common class instead of ids and multiple classes. Use multiple classes if required.
* Naming - Give descriptive names. If you can't think of a name immediately, think through the process. In the real world, you spend more time reading code than writing code. So names matter.
* Use `em` or `rem` for font-sizes instead of `px`, `vw` etc.
* Use `%`, `em` or `rem` for margins and paddings instead of `px`

## Debugging

* Build a rough layout of the entire page before diving into the details.
* Test components in isolation.
* If you're not sure how a property works, read about it on MDN, CSS Tricks, StackOverflow etc.
* Use Chrome Dev Tools. It should always be open throughout your web development career.

### git

* Commit your work after adding each component/feature
* Write descriptive commit messages. (Good: 'Add headlines'. Bad: 'Made change')


