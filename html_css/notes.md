
https://en.wikipedia.org/wiki/Cascading_Style_Sheets

> The name cascading comes from the specified priority scheme to determine which style rule applies if more than one rule matches a particular element. This cascading priority scheme is predictable.



## em unit

https://en.wikipedia.org/wiki/Em_(typography)

* rem vs em
* line height

___________

## fonts

* https://fonts.google.com/
* serif, sans-serif, monospace

_________________


## Selectors

* Type selector
* Class selector - When do we use class? Combining classes.
* id selector
* Child selectors
* Descendent selector

_________

## block vs inline vs inline-block

https://stackoverflow.com/questions/9189810/css-display-inline-vs-inline-block

Inline elements:

1. respect left & right margins and padding, but **not** top & bottom
2. **cannot** have a width and height set
3. allow other elements to sit to their left and right.
4. see very important side notes on this [here][1].

Block elements:

1. respect all of those
2. force a line break after the block element
3. acquires full-width if width not defined

Inline-block elements:

1. allow other elements to sit to their left and right
2. respect top & bottom margins and padding
3. respect height and width

_________

## position

https://developer.mozilla.org/en-US/docs/Web/CSS/position

**relative**
> The element is positioned according to the normal flow of the document, and then offset relative to itself based on the values of top, right, bottom, and left. The offset does not affect the position of any other elements; thus, the space given for the element in the page layout is the same as if position were static.
_________

## box-sizing

## Flexbox

* https://css-tricks.com/snippets/css/a-guide-to-flexbox/

## Other resources

* https://caniuse.com/
