# Transactions

## What is ACID:

* https://www.essentialsql.com/what-is-meant-by-acid/
* https://stackoverflow.com/questions/3740280/acid-and-database-transactions
* https://en.wikipedia.org/wiki/ACID_(computer_science)


## Isolation levels:

* https://en.wikipedia.org/wiki/Isolation_(database_systems)
* https://vladmihalcea.com/a-beginners-guide-to-acid-and-database-transactions/
* https://www.geeksforgeeks.org/transaction-isolation-levels-dbms/


