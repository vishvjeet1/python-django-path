## Advanced SQL resources:

https://stackoverflow.com/questions/2054130/what-is-advanced-sql


## Functions - https://dev.mysql.com/doc/refman/8.0/en/functions.html

## Stored Procedure
- https://en.wikipedia.org/wiki/Stored_procedure
- http://www.mysqltutorial.org/introduction-to-sql-stored-procedures.aspx
- https://dev.mysql.com/doc/connector-net/en/connector-net-tutorials-stored-procedures.html

## Hierarchical Queries

- https://pgexercises.com/questions/recursive/ (This is a Postgres example. Use MySQL to execute your queries instead)
- https://en.wikipedia.org/wiki/Hierarchical_and_recursive_queries_in_SQL


## Triggers

- http://www.mysqltutorial.org/sql-triggers.aspx
- http://www.mysqltutorial.org/mysql-trigger-implementation.aspx
- http://www.mysqltutorial.org/managing-trigger-in-mysql.aspx


## Dynamic SQL

- https://stackoverflow.com/questions/4165020/what-is-dynamic-sql

## Views

- https://en.wikipedia.org/wiki/Materialized_view
- https://stackoverflow.com/questions/93539/what-is-the-difference-between-views-and-materialized-views-in-oracle
- http://www.mysqltutorial.org/introduction-sql-views.aspx
- http://www.mysqltutorial.org/mysql-views.aspx
- http://www.mysqltutorial.org/create-sql-views-mysql.aspx
- http://www.mysqltutorial.org/create-sql-updatable-views.aspx


## Query Optimization:

* Indexes
* Query Optimization: Explain Plans
* Query Optimization: Profiling

