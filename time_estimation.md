## Time estimation

* https://softwareengineering.stackexchange.com/questions/16326/how-to-learn-to-make-better-estimates

> I'm still not great at it, but I have found that tracking how long you estimate for tasks and how long you actually take can be a big help. That way you can get a solid idea of how far off you usually are. Issue management software with time tracking (Jira in my case) or a spread sheet can be a big help with this.


> Without historical information, your estimates are useless.


* https://coding.abel.nu/2012/06/programmer-time-translation-table/

* https://softwareengineering.stackexchange.com/questions/145865/dealing-with-estimates-as-a-junior-programmer

___________

Things to be considered when providing an estimate when adding a feature / fixing a bug.

* What do I need to learn?
    - A new technology?
    - How to use a library?
    - A new programming concept / design pattern.
    - A new domain?
    - How the system works?

* Do I have to explore a new part of the codebase?

* Do I have to setup a database?

* Debugging

* Time taken to add tests


Break down your task into smaller tasks and estimate how much time you'll take to complete each task and then estimate the time taken to complete the entire task.
