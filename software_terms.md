### Software terms assignment

* 10 popular Linux commands - daily use
* 10 popular network commands
* 10 popular OS commands
* Database
* SQL Database
* NoSQL Database
* 10 popular databases
* ACID
* Aggregations
* Joins
* CAP Theorem
* 7 network layers
* Request Response Protocol
* HTTP
* TCP 
* UDP
* Web server
* Static server
* Application server
* DNS server
* Database Server
* Standalone Application
* MVC
* Operating System
* Kernel
* Process
* Thread
* Apache Web Server
* Nginx Web Server
* Messaging Queue
* Enterprise Message Bus
* RabbitMQ
* Kafka
* Zookeeper
* Service Oriented Architecture
* Microservices Architecture
* Redis
* Solr
* ElasticSearch
* Celery
* Node JS
* MongoDB
* Django
* Django REST Framework
* Progressive Web Apps (PWA)
* Session Based Authentication
* Token Based Authentication
* Authorization
* Docker
* IaaS
* AWS
* PaaS
* Heroku
