## Deploy your Flask project on an EC2 instance


**Note**: Disable Elasticsearch in production using a suitable environment variable since it takes up too much RAM. 

## Part 1.1 - Basic deployment

**Goal**: Get the project running in development mode on EC2. 

* Install Postgres, Redis, Python3, virtualenv etc. 
* Clone the git repo
* Install the requirements
* Run the migrations
* Start the development Flask server
* Start the Celery worker
* Check if everything works!
________


## Part 1.2 - Production deployment

**Goal**: Get the project running on production mode

* Install Nginx
* Install gunicorn

* Configure Nginx so that it forwards HTTP requests to gunicorn
* Run gunicorn as a systemd service
* Run the background worker as a systemd service
* Log all the HTTP requests to gunicorn in a file called `access.log`
* Log all the application errors in a file called `error.log`


___________


## Resources

Choose one yourself!

https://www.google.com/search?q=deploy+flask+nginx+gunicorn


## Guidelines

* Save all your commands in a shell script called `deploy.sh`, so that the entire application can be deployed in a shell script
* Add `deploy.sh` to your git repo
