## Drills - Web development with Flask

We'll use a Library App as an example

### Task - Initialize the project

* Create a directory
* Add a gitignore
* Initialize the git repo
* Create a Flask app
* Set up environment variables 
    - FLASK_DEBUG=1
* Set up connection to database
_________________



### Task - Create the following models with the following fields

We have the following entities

* Book - id, user_id, title
* Author - id, name
* Category - id, name
* BookCategory - book_id, category_id
* BookAuthor - book_id, author_id

- A book can belong to one or more categories
- A book can be written by one or more authors


* Create the above models in a `models.py` file
* Initialize the database
* Migrate the database
_____________

### Task - Create some sample data using Flask shell

* Create a user
* Create 10 books. 
* Create 10 categories.
* Create 10 authors
* Add 2 to 3 categories to each book
* Add 1 to 3 authors to each book

(Don't enter each row manually. Instead define a Python module which inserts random data)

______________

### Drill - SQLAlchemy

Using the Flask shell write SQL queries for the following:

* Fetch book having id = 1
* Fetch books having ids in [1, 2, 3] in one query
* Fetch book details for book_id=2: Book, categories, authors
* Fetch all books belonging to a particular category

______________


### Drill  - CRUD

1. Book Detail - GET

* Create a route function `book_detail` having url `/books/<book_id>`
* Render Book details - Book title, author names, categories

2. Book Detail - POST

Constraint: Don't use `form.validate_on_submit`

* Create a route function `create_book` having url `/books/new`
* Ensure only logged in users can access this URL
* When user makes a GET request, show a form having following fields:
    - book title,
    - categories (input field. Multiple categories can be entered separated by commas)
    - authors (input field. Multiple authors can be entered separated by commas)
* When user submits a form, a POST request should be made.
* Save the form. Set the book's `user_id` field to the user who made the HTTP request.
* If a category or author is not already present in the database, insert corresponding entries for each new category and author. 
* After saving form, redirect user to corresponding `book_detail` page


3. Book List: - GET

* Create a route function `book_list` having url `/books`
* Render all books
* In the template, use url_for to link to each book's detail page

4. url_for

* What are the arguments accepted by `url_for`. What does it return?
* Use the flask_shell and call url_for with various parameters.

5. Use the Flask shell and list all the routes

6. Chrome Dev Tools

* Open the Network tab. Use all the features you've implemented so far. Understand every request and response header, form data, status_code etc. 

_________

### Visual Debugger

* Learn to use PyCharm or VSCode's debugger.
* Put breakpoints.
* Learn to use step out, step over, step in
* Inspect all the variables
* Learn what a call stack is. 

_______________

### Read Only mode

Imagine your servers are under heavy load!! So you want to put your website in read-only mode, i.e. users can only read existing content, but can't create books. 

How will you do that by writing the minimum possible, elegant code?

________________

### Cookies

* Log in to your project
* Fetch the user_id manually from the session_cookie
* Set a custom cookie called as "hello" and set its value to "world" (Observe Chrome dev tools)

