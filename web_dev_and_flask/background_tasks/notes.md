# Background tasks

First, understand what Celery does:

* http://docs.celeryproject.org/en/latest/getting-started/introduction.html
* http://docs.celeryproject.org/en/latest/getting-started/first-steps-with-celery.html
* https://www.vinta.com.br/blog/2017/celery-overview-archtecture-and-how-it-works/


The mechanics of how to implement background tasks using Flask is well explained by Miguel Grinberg

* https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xxii-background-jobs
* https://blog.miguelgrinberg.com/post/using-celery-with-flask

**Note**: We'll stick to AJAX polling instead of Websockets in this project.

________


### Download user's content using a celery worker

Add a feature to download all the user's question into a JSON file. 

Since this is a computationally intensive task, it must be performed in the background. 

__________


### High level Workflow:

1. Create a `url` called `/download_content`
2. When user visits this url (i.e, makes a `GET` request), show a button called "Download Your Content"
3. When user clicks on this button, make an AJAX POST request to `/download_content`
4. On the Flask server, start an async task to download the user's questions and return the `task_id` as part of the HTTP Response
5. In the background, run a celery worker that accepts the task and stores the data in a file.
6. On the browser, periodically check whether the download task is completed by polling `/download_content/<task_id>` using jQuery
7. If the task is completed, trigger the download using JavaScript
8. On the server, when you receive a `GET` request to `/download_content/content.json`, respond with the json file data.

______



