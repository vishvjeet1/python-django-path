# Drills

## Local commands

### Part 1 - Merging

* Create a directory called `git_exercise`.

* `cd` to `git_exercise`. 

* Initialize a `git` repository at `git_exercise`

* Create a file called `one.py` and add the following contents to the file:

```
print('one')
x = [1, 2, 3]
y = [4, 5, 6]
```

* Commit `one.py` with the message "Initial message"

* Add another file called `two.py` with the following contents

```
print("two")
```

* Execute `git status`. Understand each line printed by the command

* Create a new branch called `feature-2`

* Commit `two.py` in `feature-2`

* Go to `master` branch. Merge `feature-2`

* Create a branch named `feature-3`. 

* Create a file called `three.py` with the following contents

```
print("three")
```

* Create a file called `README.md` with the following contents

```
## Hello, world!

To run `feature-3`, execute `python3 three.py`
```


* Commit `three.py` and `README.md`

* Switch to master branch. (DON'T merge `feature-3` now)

* Create a new branch named `feature-4`

* Create a file called `four.py` with the following contents

```
print("four")
```

* Create a file called `README.md` with the following contents

```
## Hello, world!

To run `feature-4`, execute `python3 three.py`
```

* Commit `four.py` and `README.md`

* Switch to master branch

* Merge `feature-3`

* Merge `feature-4`. Fix the merge conflicts

`README.md` must contain:

```
## Hello, world!

To run `feature-3`, execute `python3 three.py`
To run `feature-4`, execute `python3 four.py`
```

* Run git status

* Commit the files that need aren't committed yet.


### Part 2 - Rebase

* Create a branch named `feature-5`. 

* Create a file called `five.py` with the following contents

```
print("five")
```

* Edit `README.md` so that it has the following contents

```
## Hello, world!

To run `feature-3`, execute `python3 three.py`
To run `feature-4`, execute `python3 four.py`
To run `feature-5`, execute `python3 five.py`
```


* Commit `five.py` and `README.md`

* Switch to master branch. (DON'T merge `feature-5` now)

* Create a new branch named `feature-6`

* Create a file called `six.py` with the following contents

```
print("six")
```

* Create a file called `README.md` with the following contents

```
## Hello, world!

To run `feature-3`, execute `python3 three.py`
To run `feature-4`, execute `python3 four.py`
To run `feature-6`, execute `python3 six.py`
```

* Commit `six.py` and `README.md`

* Switch to master branch

* Merge `feature-5`

* Switch to `feature-6` branch

* Rebase `feature-6` from `master`

* Fix the merge conflicts

`README.md` must contain:

```
## Hello, world!

To run `feature-3`, execute `python3 three.py`
To run `feature-4`, execute `python3 four.py`
To run `feature-5`, execute `python3 five.py`
To run `feature-6`, execute `python3 six.py`
```

* Run git status

* Commit the files that need aren't committed yet.

* Switch to `master`

* Merge `feature-6`

* Create a repo on Gitlab.

* Set `origin` to your Gitlab repo.

* Push `master`, `feature-2`, `feature-3`, `feature-4`, `feature-5`,
`feature-6` to `origin`

* Send me a link to the your Gitlab repo.

